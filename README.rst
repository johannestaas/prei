prei
====

Python Remote Execution Interface

Installation
------------

From the project root directory::

    $ python setup.py install

Usage
-----

Simply run it::

    $ prei

Use --help/-h to view info on the arguments::

    $ prei --help

Release Notes
-------------

:0.0.2:
    Fix issue where it locked on running commands that are closed
:0.0.1:
    Project created
