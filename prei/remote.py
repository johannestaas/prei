import inspect
from .client import Client


class RemoteTool:

    def __init__(self):
        self.tasks = {}
        self.task_names = {}
        self.connections = {}
        self.remote_scripts = {}
        self.remote_script_dir = {}
        self.local_scripts = {}
        self.local_script_paths = set()
        self.func_code = {}

    def _signature(self, func):
        return '{}.{}'.format(func.__module__, func.__qualname__)

    def task(self, func):
        self.task_names[func.__name__] = func
        self.task_names[func.__qualname__] = func
        self.tasks[func.__qualname__] = func
        func_path = inspect.getfile(func)
        self.local_scripts[func.__qualname__] = func_path
        self.local_script_paths.add(func_path)
        src = inspect.getsource(func)
        # Strips out the @task bit in a hacky way.
        src = '\n'.join(src.splitlines()[1:])
        self.func_code[func.__qualname__] = src
        return func

    def connect(self, *hosts, **kwargs):
        if not hosts:
            raise ValueError('hosts must be defined')
        self.connections = self.connections or {}
        for host in hosts:
            remote_dir = self.remote_script_dir.get(host)
            self.remote_scripts[host] = {}
            if self.connections.get(host):
                client = self.connections[host]
            else:
                client = Client()
                self.connections[host] = client
                client.connect(host, **kwargs)
            for script in self.local_script_paths:
                path, new_dir = client.rsync_to_tempfile(script,
                                                         dir=remote_dir)
                self.remote_script_dir[host] = new_dir
                self.remote_scripts[host][script] = path

    def disconnect(self, hosts=None):
        hosts = hosts or self.connections.keys()
        for host in hosts:
            script = self.remote_scripts.get(host)
            if script:
                self.connections[host].call('rm {}'.format(script))
            self.connections[host].disconnect()

    def __getitem__(self, host):
        return self.connections.get(host)

    def funcname_to_localpath(self, name):
        func = self.task_names.get(name)
        if not func:
            return None
        return self.local_scripts.get(func.__qualname__)

    def funcname_to_remotepath(self, name, host):
        localpath = self.funcname_to_localpath(name)
        if localpath is None:
            return None
        return self.remote_scripts[host].get(localpath)

    def run_remote_python_function(self, func, host):
        # cmd = 'eval "cd /tmp/tmp.kKrQQHL46Q && python -c 'from foo import bar ; bar()' && cd -"'
        pass

    def run(self, task_name, hosts=None, args=None, kwargs=None):
        if hosts is None:
            hosts = list(self.connections.keys())
        if not hosts:
            return None

        func = self.task_names.get(task_name)
        # local_script_path = self.funcname_to_localpath(task_name)
        # remote_path = self.funcname_to_remotepath(task_name)

        for host in hosts:
            self.run_remote_python_function(func, host)
