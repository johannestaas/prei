import re
import os
import sys
import shutil
import subprocess
from contextlib import contextmanager


RE_SCRIPT_NAME = re.compile(r'(\w[-a-zA-Z0-9_]*)\.[pP][yY]')


SETUP_TEXT = '''from setuptools import setup
setup(
    name="{name}",
    version="0.0.1",
    description='{description}',
    packages=['{name}'],
    package_dir={{'{name}': '{name}'}},
    long_description='',
    classifiers=[],
    install_requires={requirements!r},{entry_points}
)
'''

ENTRY_POINTS_TEXT = '''
    entry_points={{
        'console_scripts': [
            '{name}={name}:{main}',
        ],
    }},
'''

MANIFEST_IN_TEXT = '''recursive-include {name} *.py\n'''


@contextmanager
def cd(dest_dir):
    old_dir = os.getcwd()
    os.chdir(os.path.expanduser(dest_dir))
    try:
        # code block in context
        yield
    finally:
        # return back to old dir
        os.chdir(old_dir)


def create_package_name(path):
    fname = os.path.basename(path)
    name_match = RE_SCRIPT_NAME.match(fname)
    if not name_match:
        raise ValueError('script from path {!r} does not have a package name '
                         'that can be created from it (doesnt match *.py)'
                         .format(path))
    name = name_match.group(1)
    name = name.replace('-', '_')
    name = name.lower()
    return name


def build_setup(name, entry_point=None, description=None, requirements=None):
    requirements = list(requirements or [])
    if entry_point:
        entry_text = ENTRY_POINTS_TEXT.format(name=name, main=entry_point)
    else:
        entry_text = ''
    return SETUP_TEXT.format(
        name=name,
        description=description or '',
        requirements=requirements,
        entry_points=entry_text,
    )


def package_script(path, package_name=None, outdir=None, entry_point=None,
                   description=None, outtar=None):
    if package_name is None:
        try:
            package_name = create_package_name(path)
        except ValueError as e:
            sys.exit(str(e))
    if outdir is None:
        outdir = package_name
    if os.path.exists(outdir):
        sys.exit('Please remove directory {!r} and run again.'.format(outdir))
    moddir = os.path.join(outdir, package_name)
    os.makedirs(moddir)
    init_path = os.path.join(moddir, '__init__.py')
    shutil.copy(path, init_path)
    setup_text = build_setup(package_name, entry_point=entry_point,
                             description=description or '')
    manifest_text = MANIFEST_IN_TEXT.format(name=package_name)
    setup_path = os.path.join(outdir, 'setup.py')
    manifest_path = os.path.join(outdir, 'MANIFEST.in')
    with open(setup_path, 'w') as f:
        f.write(setup_text)
    with open(manifest_path, 'w') as f:
        f.write(manifest_text)
    with cd(outdir):
        subprocess.call(['python', 'setup.py', 'sdist'])
    dist_name = '{name}-0.0.1.tar.gz'.format(name=package_name)
    dist_path = os.path.join(outdir, 'dist', dist_name)
    outtar = outtar or '{name}.tar.gz'.format(name=package_name)
    return shutil.copy(dist_path, outtar)


def cli_package(args):
    path = package_script(
        args.path, package_name=args.name, outdir=args.outdir,
        entry_point=args.main_function, description=args.description,
        outtar=args.out)
    print('Packaged {} to {}'.format(args.path, path))
