'''
prei

Python Remote Execution Interface
'''

__title__ = 'prei'
__all__ = ('Client')
__version__ = '0.0.2'
__author__ = 'Johan Nestaas <johannestaas@gmail.com>'
__license__ = 'GPLv3+'
__copyright__ = 'Copyright 2017 Johan Nestaas'

from .client import Client


def main():
    from .packager import cli_package
    import argparse
    parser = argparse.ArgumentParser()
    subs = parser.add_subparsers(dest='cmd')
    p = subs.add_parser('package')
    p.add_argument('path')
    p.add_argument('--name', '-n')
    p.add_argument('--outdir', '-o')
    p.add_argument('--main-function', '-m')
    p.add_argument('--description', '-d')
    p.add_argument('--out', '-O')
    args = parser.parse_args()
    if args.cmd == 'package':
        cli_package(args)
    else:
        parser.print_usage()


if __name__ == '__main__':
    main()
